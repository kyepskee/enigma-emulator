package main

import ()

type Rotor struct {
	perm                          Permutation
	offset, length, turnoverPoint int
}

func NewRotor(mapping []int, turnoverPoint, offset int) *Rotor {
	rotor := new(Rotor)
	rotor.perm = NewPermutation(mapping)
	rotor.turnoverPoint = turnoverPoint
	rotor.length = len(mapping)
	return rotor
}

func CopyRotor(rotor Rotor) *Rotor {
	newRotor := new(Rotor)
	newRotor.perm = rotor.perm
	newRotor.offset = rotor.offset
	newRotor.length = rotor.length
	newRotor.turnoverPoint = rotor.turnoverPoint
	return newRotor
}

func (rotor Rotor) Get(index int) int {
	// also add the length in case it maps it to a negative number
	return rotor.perm.Apply((rotor.offset + index + rotor.length) % rotor.length)
}

func (rotor Rotor) GetReverse(index int) int {
	// also add the length in case it maps it to a negative number
	return rotor.perm.ApplyReverse((rotor.offset + index + rotor.length) % rotor.length)
}

func (rotor *Rotor) Step(index int) bool {
	// TODO: potential point of failure;
	// 		 not sure if turnover happens after or during the letter press
	(*rotor).offset++

	if rotor.offset-1 == rotor.turnoverPoint {
		return true
	}
	return false
}
