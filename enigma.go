package main

import (
	"fmt"
	"log"
	"os"
)

var Rotors [5]Rotor
var Plugboard Permutation
var Reflector Permutation

func init() {
	// a -> a -> e -> s -> g -> l
	rotorStrings := map[int]string{
		//  ABCDEFGHIJKLMNOPQRSTUVWXYZ
		1: "EKMFLGDQVZNTOWYHXUSPAIBRCJ",
		2: "AJDKSIRUXBLHWTMCQGZNPYFVOE",
		3: "BDFHJLCPRTXVZNYEIWGAKMUSQO",
		4: "ESOVPZJAYQUIRHXLNFTGKDCMWB",
		5: "VZBRGITYUPSDNHLXAWMJQOFECK",
		// 6: "JPGVOUMFYQBENHZRDKASXLICTW",
		// 7: "NZJHGRCXMYSWBOUFAIVLPEKQDT",
		// 8: "FKQHTLXOCBJSPDZRAMEWNIUYGV",
	}
	rotorTurnovers := map[int]byte{
		1: 'Q',
		2: 'E',
		3: 'V',
		4: 'J',
		5: 'Z',
	}

	for k, rotorString := range rotorStrings {
		mapping := make([]int, 26)
		for i, v := range rotorString {
			mapping[i] = upperCaseToAlphabetIndex(byte(v))
		}
		rotorTurnoverPoint := upperCaseToAlphabetIndex(rotorTurnovers[k])
		Rotors[k-1] = *NewRotor(mapping, rotorTurnoverPoint, 0)
	}

	Plugboard = UniformPermutation(26)

	reflectorMaps := []string{"AY", "BR", "CU", "DH", "EQ", "FS", "GL", "IP", "JX", "KN", "MO", "TZ", "VW"}
	mapping := make([]int, 26)
	for _, reflectorMap := range reflectorMaps {
		letterOneIndex, letterTwoIndex := upperCaseToAlphabetIndex(reflectorMap[0]), upperCaseToAlphabetIndex(reflectorMap[1])
		mapping[letterOneIndex] = letterTwoIndex
		mapping[letterTwoIndex] = letterOneIndex
	}
	Reflector = NewPermutation(mapping)
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Wrong number of arguments.")
		return
	}

	rotorIds := []int{2, 1, 0}
	rotors := []Rotor{Rotors[rotorIds[0]], Rotors[rotorIds[1]], Rotors[rotorIds[2]]}
	fmt.Println(rotors[0])
	fmt.Println(rotors[1])
	fmt.Println(rotors[2])
	fmt.Println(Reflector)
	for _, letter := range os.Args[1] {
		fmt.Println(string(Press(byte(letter), Plugboard, []*Rotor{&rotors[0], &rotors[1], &rotors[2]}, Reflector)))
	}
}

func Press(letter byte, plugboard Permutation, rotors []*Rotor, reflector Permutation) rune {
	letterIndex := lowerCaseToAlphabetIndex(letter)

	result := plugboard.Apply(letterIndex)
	fmt.Println("result here:	", result, "  ", string(result+97))

	for _, rotor := range rotors {
		result = rotor.Get(result)
		fmt.Println("result here:	", result, "  ", string(result+97))
	}
	result = reflector.Apply(result)
	fmt.Println("result here:	", result, "  ", string(result+97))
	last := len(rotors) - 1
	for i := last; i >= 0; i-- {
		result = rotors[i].GetReverse(result)
		fmt.Println("result here:	", result, "  ", string(result+97))
	}
	result = plugboard.ApplyReverse(result)
	fmt.Println("result here:	", result, "  ", string(result+97))

	return rune(result + 65)
}

func lowerCaseToAlphabetIndex(r byte) int {
	return int(r - 97)
}

func upperCaseToAlphabetIndex(r byte) int {
	return int(r - 65)
}
