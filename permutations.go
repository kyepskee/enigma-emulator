package main

import (
	"math/rand"
)

type Permutation struct {
	length                  int
	mapping, mappingReverse []int
}

// initialize with an "absolute" mapping - values aren't relative
// zero-indexed
func NewPermutation(mapping []int) Permutation {
	relativeMapping := make([]int, len(mapping))
	for i, v := range mapping {
		relativeMapping[i] = v - i
	}

	perm := *new(Permutation)
	perm.length = len(mapping)
	// spend 30 minutes because i assigned mapping instead of relativeMapping
	perm.mapping = relativeMapping
	perm.mappingReverse = make([]int, perm.length)
	for i, v := range relativeMapping {
		perm.mappingReverse[(i+v)%perm.length] = -v
	}
	return perm
}

func Random(length, turnoverPoint int) Permutation {
	mapping := rand.Perm(length)
	// subtract 1 from every element as the permutation is 1 to n instead of 0 to n-1
	for i, _ := range mapping {
		mapping[i]--
	}
	return NewPermutation(mapping)
}

func UniformPermutation(length int) Permutation {
	mapping := make([]int, length)

	// zero out
	for i, _ := range mapping {
		mapping[i] = i
	}

	return NewPermutation(mapping)
}

func (perm Permutation) Apply(index int) int {
	return (index + perm.mapping[index] + perm.length) % perm.length
}

func (perm Permutation) ApplyReverse(index int) int {
	return (index + perm.mappingReverse[index] + perm.length) % perm.length
}
